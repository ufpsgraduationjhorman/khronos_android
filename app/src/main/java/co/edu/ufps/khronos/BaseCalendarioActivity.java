package co.edu.ufps.khronos;

/**
 * Created by jhorapb on 14/01/18.
 */


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.RectF;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Response;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import co.edu.ufps.khronos.adapters.RestAPIAdapter;
import co.edu.ufps.khronos.models.ExamenCalendarioResponse;
import co.edu.ufps.khronos.restapi.Endpoints;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * This is a base activity which contains week view and all the codes necessary to initialize the
 * week view.
 * Created by Raquib-ul-Alam Kanak on 1/3/2014.
 * Website: http://alamkanak.github.io
 */
public abstract class BaseCalendarioActivity extends AppCompatActivity implements
        WeekView.EventClickListener, MonthLoader.MonthChangeListener,
        WeekView.EventLongPressListener, WeekView.EmptyViewLongPressListener {

    private static final int TYPE_DAY_VIEW = 1;
    private static final int TYPE_THREE_DAY_VIEW = 2;
    private static final int TYPE_WEEK_VIEW = 3;
    private int widgetWeekViewType = TYPE_THREE_DAY_VIEW;
    private WeekView widgetWeekView;

    private static final int RC_SIGN_IN = 0;
    private static final String TAG = "LoginActivity";
    private SharedPreferences prefs;
    //public List<ExamenCalendarioResponse> examenesCalendario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendario);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        // Get a reference for the week view in the layout.
        widgetWeekView = (WeekView) findViewById(R.id.calendarioExamenesWeekView);

        // Show a toast message about the touched event.
        widgetWeekView.setOnEventClickListener(this);

        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.

        ///widgetWeekView.getMonthChangeListener();
        widgetWeekView.setMonthChangeListener(this);

        // Set long press listener for events.
        widgetWeekView.setEventLongPressListener(this);

        // Set long press listener for empty view
        widgetWeekView.setEmptyViewLongPressListener(this);

        widgetWeekView.goToHour(6);

        // Set up a date time interpreter to interpret how the date and time will be formatted in
        // the week view. This is optional.
        setupDateTimeInterpreter(false);

        Toolbar actionBar = (Toolbar) findViewById(R.id.actionBar);

        setSupportActionBar(actionBar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        /*
        actionBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLoginActivity();
            }
        });
        */
        Bundle params = getIntent().getExtras();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        setupDateTimeInterpreter(id == R.id.action_week_view);
        switch (id) {
            case R.id.action_today:
                widgetWeekView.goToToday();
                return true;
            case R.id.action_day_view:
                if (widgetWeekViewType != TYPE_DAY_VIEW) {
                    item.setChecked(!item.isChecked());
                    widgetWeekViewType = TYPE_DAY_VIEW;
                    widgetWeekView.setNumberOfVisibleDays(1);

                    // Lets change some dimensions to best fit the view.
                    widgetWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                    widgetWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                    widgetWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                }
                return true;
            case R.id.action_three_day_view:
                if (widgetWeekViewType != TYPE_THREE_DAY_VIEW) {
                    item.setChecked(!item.isChecked());
                    widgetWeekViewType = TYPE_THREE_DAY_VIEW;
                    widgetWeekView.setNumberOfVisibleDays(3);

                    // Lets change some dimensions to best fit the view.
                    widgetWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                    widgetWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                    widgetWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                }
                return true;
            case R.id.action_week_view:
                if (widgetWeekViewType != TYPE_WEEK_VIEW) {
                    item.setChecked(!item.isChecked());
                    widgetWeekViewType = TYPE_WEEK_VIEW;
                    widgetWeekView.setNumberOfVisibleDays(7);

                    // Lets change some dimensions to best fit the view.
                    widgetWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));
                    widgetWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                    widgetWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                }
                return true;
            case R.id.action_month_view:
                abrirVistaMes();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Abre una nueva activity con vista de meses e indicadores de exámenes.
     */
    private void abrirVistaMes(){
        Intent intent = new Intent(this, CalendarioMesActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Set up a date time interpreter which will show short date values when in week view and long
     * date values otherwise.
     *
     * @param shortDate True if the date values should be short.
     */
    private void setupDateTimeInterpreter(final boolean shortDate) {
        widgetWeekView.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(Calendar date) {
                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE", new Locale("es", "ES"));
                String weekday = weekdayNameFormat.format(date.getTime());
                String formato = shortDate ? " d MMM" : " d MMM, yy";
                SimpleDateFormat format = new SimpleDateFormat(formato, new Locale("es", "ES"));

                // All android api level do not have a standard way of getting the first letter of
                // the week day name. Hence we get the first char programmatically.
                // Details: http://stackoverflow.com/questions/16959502/get-one-letter-abbreviation-of-week-day-of-a-date-in-java#answer-16959657
                if (shortDate)
                    weekday = String.valueOf(weekday.charAt(0));
                return weekday.toUpperCase() + format.format(date.getTime());
            }

            @Override
            public String interpretTime(int hour) {
                return hour > 12 ? (hour - 12) + " PM" : hour == 12 ? "12 PM" : (hour == 0 ? "12 AM" : hour + " AM");
            }
        });
    }

    /*
    protected String getEventTitle(Calendar time) {
        return String.format("Event of %02d:%02d %s/%d", time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH));
    }
    */

    protected String getEventTitle(String nombreExamen, String tipoExamen) {
        return String.format(Locale.getDefault(), "%s (%s)", nombreExamen, tipoExamen);
    }

    protected String getEventTimeTitle(Calendar time) {
        return String.format("Evento %02d:%02d %s/%d", time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        Toast.makeText(this, "Evento: " + event.getName(), Toast.LENGTH_SHORT).show();
        mostrarDetalleExamen(event);
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
        Toast.makeText(this, "Evento: " + event.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEmptyViewLongPress(Calendar time) {
        Toast.makeText(this, "No hay exámenes programados en este horario: " + getEventTimeTitle(time),
                Toast.LENGTH_SHORT).show();
    }

    public WeekView getWeekView() {
        return widgetWeekView;
    }

    public SharedPreferences getPrefs() {
        return prefs;
    }

    public void mostrarDetalleExamen(WeekViewEvent examen){
        Intent intent = new Intent(this.getApplicationContext(), DetalleExamenActivity.class);

        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a", new Locale("es", "ES"));

        intent.putExtra(getResources().getString(R.string.param_examen_materia),
                examen.getName());
        intent.putExtra(getResources().getString(R.string.param_examen_hora_inicio),
                dateFormat.format(examen.getStartTime().getTime()));
        intent.putExtra(getResources().getString(R.string.param_examen_hora_fin),
                dateFormat.format(examen.getEndTime().getTime()));
        intent.putExtra(getResources().getString(R.string.param_examen_salon),
                examen.getLocation());
        startActivity(intent);
        finish();
    }
}
