package co.edu.ufps.khronos;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

/**
 * Created by jhorapb on 11/07/16.
 */
public class DetalleExamenActivity extends AppCompatActivity {

    private TextView textViewMateria;
    private TextView textViewHoraInicio;
    private TextView textViewHoraFin;
    private TextView textViewSalon;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_examen);

        Toolbar actionBar = (Toolbar) findViewById(R.id.actionBar);
        setSupportActionBar(actionBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        actionBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCalendarioPrincipalActivity();
            }
        });

        Bundle params = getIntent().getExtras();
        String nombreMateriaExamen = params.getString(getResources().getString(R.string.param_examen_materia));
        String horaInicioExamen = params.getString(getResources().getString(R.string.param_examen_hora_inicio));
        String horaFinExamen = params.getString(getResources().getString(R.string.param_examen_hora_fin));
        String salonExamen = params.getString(getResources().getString(R.string.param_examen_salon));

        textViewMateria = (TextView) findViewById(R.id.textViewMateria);
        textViewHoraInicio = (TextView) findViewById(R.id.textViewHoraInicio);
        textViewHoraFin = (TextView) findViewById(R.id.textViewHoraFin);
        textViewSalon = (TextView) findViewById(R.id.textViewSalon);

        textViewMateria.setText(nombreMateriaExamen);
        textViewHoraInicio.setText("Inicio: " + horaInicioExamen);
        textViewHoraFin.setText("Fin: " + horaFinExamen);
        textViewSalon.setText("Salón: " + salonExamen);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if(keyCode == KeyEvent.KEYCODE_BACK){
            openCalendarioPrincipalActivity();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void openCalendarioPrincipalActivity() {
        Intent intentMain = new Intent(this, CalendarioPrincipalActivity.class);
        startActivity(intentMain);
    }
}
