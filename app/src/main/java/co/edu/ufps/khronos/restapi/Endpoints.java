package co.edu.ufps.khronos.restapi;

import com.prolificinteractive.materialcalendarview.CalendarDay;

import co.edu.ufps.khronos.models.ExamenCalendarioResponse;
import co.edu.ufps.khronos.models.UsuarioResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by jhorapb on 22/11/17.
 */

public interface Endpoints {

    @FormUrlEncoded
    @POST(ConstantesRestAPI.KEY_POST_ID_TOKEN)
    Call<UsuarioResponse> autenticarUsuario(@Field("id_token") String id_token,
                                            @Field("registration_id") String registration_id,
                                            @Field("rol_usuario") String rol_usuario,
                                            @Field("type") String type);

    @GET(ConstantesRestAPI.GET_CALENDARIO_EXAMENES)
    Call<ExamenCalendarioResponse[]> leerCalendario(@Query("auth_token_app") String auth_token_app);

    @GET(ConstantesRestAPI.GET_CALENDARIO_EXAMENES)
    Call<ExamenCalendarioResponse[]> leerCalendario2(@Query("usuario_id") long usuario_id);

    @GET(ConstantesRestAPI.GET_CALENDARIO_EXAMENES)
    Call<ExamenCalendarioResponse[]> leerExamenesFecha(@Query("usuario_id") long usuario_id,
                                                       @Query("fecha_presentacion") String fecha_presentacion);

}
