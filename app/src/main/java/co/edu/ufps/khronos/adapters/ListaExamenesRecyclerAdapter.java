package co.edu.ufps.khronos.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import co.edu.ufps.khronos.R;
import co.edu.ufps.khronos.models.ExamenCalendarioResponse;

/**
 * Created by jhorapb on 25/11/17.
 */

public class ListaExamenesRecyclerAdapter extends RecyclerView.Adapter<ListaExamenesRecyclerAdapter.ExamenViewHolder> {
    
    private List<ExamenCalendarioResponse> datasetExamenes;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ExamenViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView textViewMateriaItem, textViewHoraInicioItem, textViewHoraFinItem;
        private CardView cardViewExamenItem;
        public ExamenViewHolder(View examenItem) {
            super(examenItem);
            //cardViewExamenItem = examenItem.findViewById(R.id.cardViewExamenItem);
            textViewMateriaItem = examenItem.findViewById(R.id.textViewMateriaItem);
            textViewHoraInicioItem = examenItem.findViewById(R.id.textViewHoraInicioItem);
            textViewHoraFinItem = examenItem.findViewById(R.id.textViewHoraFinItem);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ListaExamenesRecyclerAdapter(List<ExamenCalendarioResponse> myDataset) {
        for (ExamenCalendarioResponse examen :
                myDataset) {
            Log.d("DATASET EX", examen.toString());
        }
        datasetExamenes = myDataset;
    }

    // Crea nuevas views (invoked by the layout manager).
    @Override
    public ListaExamenesRecyclerAdapter.ExamenViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // Se crea una nueva view.
        View examenItem = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_examen_item, parent, false);
        // Se inicializan los valores: view's size, margins, paddings and layout parameters.

        return new ExamenViewHolder(examenItem);

    }

    // Reemplaza el contenido de cada view con los datos respectivos de cada elemento del dataset
    // (invoked by the layout manager).
    @Override
    public void onBindViewHolder(ExamenViewHolder examenViewHolder, int position) {
        // Get element from your dataset at this position
        // Replace the contents of the view with that element
        ExamenCalendarioResponse examen = datasetExamenes.get(position);
        String nombreExamen = String.format(Locale.getDefault(), "%s%s-%s %s",
                examen.getPlan_de_estudios(), examen.getCodigo_materia(),
                examen.getGrupo(),
                examen.getNombre_materia());
        examenViewHolder.textViewMateriaItem.setText(nombreExamen);
        examenViewHolder.textViewHoraInicioItem.setText(examen.getHora_inicio());
        examenViewHolder.textViewHoraFinItem.setText(examen.getHora_fin());
    }

    // Return the size of your dataset (invoked by the layout manager).
    @Override
    public int getItemCount() {
        return datasetExamenes.size();
    }
}