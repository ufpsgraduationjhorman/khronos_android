package co.edu.ufps.khronos.decorators;

import android.text.style.BackgroundColorSpan;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import java.util.Collection;
import java.util.HashSet;

public class EventDecorator implements DayViewDecorator {

    private final int color;
    private final int fondo;
    private final HashSet<CalendarDay> dates;

    public EventDecorator(int color, int fondo, Collection<CalendarDay> dates) {
        this.color = color;
        this.fondo = fondo;
        this.dates = new HashSet<>(dates);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return dates.contains(day);
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.addSpan(new DotSpan(5, color));
        //view.addSpan(new BackgroundColorSpan(fondo));
        //view.setBackgroundDrawable();
    }
}