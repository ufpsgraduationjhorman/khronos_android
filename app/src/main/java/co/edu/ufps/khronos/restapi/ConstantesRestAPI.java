package co.edu.ufps.khronos.restapi;

/**
 * Created by jhorapb on 22/11/17.
 */

public final class ConstantesRestAPI {

    //public static final String ROOT_URL = "http://192.168.0.22:8000/api/v1.0/"; //"http://localhost:8000/";
    public static final String ROOT_URL = "http://104.236.57.209.xip.io:8000/api/v1.0/";
    public static final String KEY_POST_ID_TOKEN = "auth-devices/";
    public static final String GET_CALENDARIO_EXAMENES = "calendario-examenes/";
    public static final String SERVER_CLIENT_ID = "737914346360-u9nh5hr8ulsbeaimhi4tr2u3g0iq507u.apps.googleusercontent.com";

}
