package co.edu.ufps.khronos;

/**
 * Created by jhorapb on 16/01/18.
 */


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.alamkanak.weekview.WeekViewEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import co.edu.ufps.khronos.adapters.RestAPIAdapter;
import co.edu.ufps.khronos.decorators.EventDecorator;
import co.edu.ufps.khronos.models.ExamenCalendarioResponse;
import co.edu.ufps.khronos.restapi.Endpoints;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * An example of how events can be fetched from network and be displayed on the week view.
 * Created by Raquib-ul-Alam Kanak on 1/3/2014.
 * Website: http://alamkanak.github.io
 */
public class CalendarioPrincipalActivity extends BaseCalendarioActivity implements Callback<ExamenCalendarioResponse[]>{

    private List<WeekViewEvent> eventsView = new ArrayList<>();
    private List<ExamenCalendarioResponse> examenesCalendario;
    boolean calledNetwork = false, irADia = true;
    private Map<Byte, Integer> infoColores = new HashMap<>();

    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {

        Bundle params = getIntent().getExtras();

        if (params != null && params.containsKey("fechaSeleccionada")){
            if (this.irADia) {
                this.irADia = false;
                getWeekView().goToDate((Calendar) params.get("fechaSeleccionada"));
            }
        }

        infoColores.put((byte) 1, R.color.colorPrimerosPrevios);
        infoColores.put((byte) 2, R.color.colorSegundosPrevios);
        infoColores.put((byte) 3, R.color.colorExamenesFinales);
        infoColores.put((byte) 4, R.color.colorHabilitacionesOpcionales);

        this.leerCalendarioExamenes();

        // Return only the events that matches newYear and newMonth.
        List<WeekViewEvent> matchedEvents = new ArrayList<WeekViewEvent>();
        for (WeekViewEvent event: this.eventsView) {
            if (eventMatches(event, newYear, newMonth)) {
                matchedEvents.add(event);
            }
        }

        //return this.eventsView;
        return matchedEvents;
    }

    private void leerCalendarioExamenes() {
        RestAPIAdapter restApi = new RestAPIAdapter();
        Endpoints endpoints = restApi.inicializarConexionRestAPI();
        Call<ExamenCalendarioResponse[]> examenCalendarioResponseCall = endpoints.leerCalendario(
                getPrefs().getString("authTokenApp", "")
        );
        examenCalendarioResponseCall.enqueue(new Callback<ExamenCalendarioResponse[]>() {
            @Override
            public void onResponse(Call<ExamenCalendarioResponse[]> call, Response<ExamenCalendarioResponse[]> response) {
                if (response.isSuccessful()){
                    examenesCalendario = Arrays.asList(response.body());
                    renderCalendario();
                    //setupRecyclerView((RecyclerView) recyclerView);
                    //progessBar.setVisibility(View.GONE);
                }
                else {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.error_calendario), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ExamenCalendarioResponse[]> call, Throwable t) {
                Log.e("FALLA REQ CALENDAR", t + "");
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.error_calendario), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void renderCalendario(){

        this.eventsView.clear();
        Map<String, String> tiposExamen = new HashMap<>();
        tiposExamen.put("1", "Primer Previo");
        tiposExamen.put("2", "Segundo Previo");
        tiposExamen.put("3", "Examen Final");
        tiposExamen.put("4", "Habilitación/Opcional");

        for (ExamenCalendarioResponse examen: this.examenesCalendario) {
            Log.v("Examen", examen.toString());
            String horaInicio [] = examen.getHora_inicio().split(":");
            String horaFin [] = examen.getHora_fin().split(":");
            Calendar startTime = Calendar.getInstance();
            startTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horaInicio[0]));
            Log.e("HORA INICIO", startTime.get(Calendar.HOUR_OF_DAY) + "");
            startTime.set(Calendar.MINUTE, Integer.parseInt(horaInicio[1]));
            Log.e("MINUTO INICIO", startTime.get(Calendar.MINUTE) + "");
            startTime.set(Calendar.DATE, examen.getFecha_presentacion().getDate());
            Log.e("DÍA INICIO", startTime.get(Calendar.DATE) + "");
            startTime.set(Calendar.MONTH, examen.getFecha_presentacion().getMonth());
            Log.e("MES INICIO", (startTime.get(Calendar.MONTH) + 1) + "");
            startTime.set(Calendar.YEAR, examen.getFecha_presentacion().getYear() + 1900);
            Log.e("AÑO INICIO", startTime.get(Calendar.YEAR) + "");

            Calendar endTime = (Calendar) startTime.clone();
            endTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horaFin[0]));
            Log.e("HORA FIN", endTime.get(Calendar.HOUR_OF_DAY) + "");
            endTime.set(Calendar.MINUTE, Integer.parseInt(horaFin[1]));
            endTime.set(Calendar.DATE, examen.getFecha_finalizacion().getDate());
            endTime.set(Calendar.MONTH, examen.getFecha_finalizacion().getMonth());
            endTime.set(Calendar.YEAR, examen.getFecha_finalizacion().getYear() + 1900);


            //AllDay event
            /*
            Calendar startTime = Calendar.getInstance();
            startTime.set(Calendar.HOUR_OF_DAY, 0);
            startTime.set(Calendar.MINUTE, 0);
            startTime.set(Calendar.MONTH, newMonth-1);
            startTime.set(Calendar.YEAR, newYear);
            Calendar endTime = (Calendar) startTime.clone();
            endTime.add(Calendar.HOUR_OF_DAY, 23);
            */
            String nombreExamen = String.format(Locale.getDefault(), "%s%s-%s %s",
                    examen.getPlan_de_estudios(), examen.getCodigo_materia(),
                    examen.getGrupo(),
                    examen.getNombre_materia());

            String tipoExamen = tiposExamen.get(examen.getTipo_examen() + "");
            WeekViewEvent event = new WeekViewEvent(examen.getId(), getEventTitle(nombreExamen,
                    tipoExamen), examen.getSalon(), startTime, endTime);
            event.setColor(ContextCompat.getColor(getApplicationContext(),
                    infoColores.get(examen.getTipo_examen())));
            event.setId(examen.getId());

            this.eventsView.add(event);
            if (!this.calledNetwork) {
                this.calledNetwork = true;
                getWeekView().notifyDatasetChanged();
            }
        }
    }

    @Override
    public void onResponse(Call<ExamenCalendarioResponse[]> call, Response<ExamenCalendarioResponse[]> response) {
        Log.d("Response cal jaja", response.toString());
        if (response.isSuccessful()){
            examenesCalendario = Arrays.asList(response.body());
            renderCalendario();
            //setupRecyclerView((RecyclerView) recyclerView);
            //progessBar.setVisibility(View.GONE);
        }
        else {
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.error_calendario), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Call<ExamenCalendarioResponse[]> call, Throwable t) {
        Log.e("FALLA REQ CALENDAR", t + "");
        Toast.makeText(getApplicationContext(),
                getResources().getString(R.string.error_calendario), Toast.LENGTH_SHORT).show();
    }

    /**
     * Checks if an event falls into a specific year and month.
     * @param event The event to check for.
     * @param year The year.
     * @param month The month.
     * @return True if the event matches the year and month.
     */
    private boolean eventMatches(WeekViewEvent event, int year, int month) {
        Calendar newTime = Calendar.getInstance();
        newTime.set(Calendar.MONTH, month);

        return (event.getStartTime().get(Calendar.YEAR) == year &&
                event.getStartTime().get(Calendar.MONTH) == newTime.get(Calendar.MONTH)) ||
                (event.getEndTime().get(Calendar.YEAR) == year &&
                event.getEndTime().get(Calendar.MONTH) == newTime.get(Calendar.MONTH));
    }

}