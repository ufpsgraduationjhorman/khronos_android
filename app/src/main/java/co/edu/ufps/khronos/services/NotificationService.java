package co.edu.ufps.khronos.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import co.edu.ufps.khronos.CalendarioMesActivity;
import co.edu.ufps.khronos.R;

/**
 * Created by jhorapb on 8/07/16.
 */
public class NotificationService extends FirebaseMessagingService {

    public static final String TAG = "FIREBASE";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        //Log.d("Título Nuevo mensaje", remoteMessage.getNotification().getTitle());
        //Log.d("Body Nuevo mensaje", remoteMessage.getNotification().getBody());
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Data: " + remoteMessage.getData());
        //Log.d(TAG, "Notification Message Body: " + remoteMessage;
        Intent i = new Intent(this, CalendarioMesActivity.class);
        this.launchNotification(remoteMessage, i);

    }

    private void launchNotification(RemoteMessage remoteMessage, Intent i){

        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        // Enlazar a CalendarioMesActivity.
        Intent resultIntent = new Intent(this, CalendarioMesActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_ONE_SHOT);

        // Acción de ignorar notificación.
        //Intent dismissIntent = new Intent(this, CalendarioMesActivity.class);
        //dismissIntent.setAction(CommonConstants.ACTION_DISMISS);
        //PendingIntent piDismiss = PendingIntent.getService(this, 0, dismissIntent, 0);

        // Otra acción de la notificación expandida.
        //Intent snoozeIntent = new Intent(this, CalendarioMesActivity.class);
        //snoozeIntent.setAction(CommonConstants.ACTION_SNOOZE);
        //PendingIntent piSnooze = PendingIntent.getService(this, 0, snoozeIntent, 0);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.date_range)
                .setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary))
                .setContentTitle(remoteMessage.getData().get("title"))
                .setContentText(remoteMessage.getData().get("body"))
                .setSound(sound)
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(
                        remoteMessage.getData().get("detalles"))
                )
                //.addAction(R.drawable.android_logo_transparent_background,
                  //    "Ignorar", piDismiss)
                //.addAction(R.drawable.android_logo_transparent_background,
                    //    "Pausar", piSnooze)//getString(R.string.snooze), piSnooze);
                ;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification.build());
    }

}
