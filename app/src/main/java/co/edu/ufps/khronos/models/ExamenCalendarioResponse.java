package co.edu.ufps.khronos.models;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by jhorapb on 24/11/17.
 */

public class ExamenCalendarioResponse {

    private long id;
    private String hora_inicio;
    private String hora_fin;
    private Date fecha_presentacion;
    private Date fecha_finalizacion;
    private byte tipo_examen;
    private String plan_de_estudios;
    private String codigo_materia;
    private String departamento_materia;
    private String nombre_materia;
    private String salon;

    private String grupo;
    private byte semestre_pensum;

    public ExamenCalendarioResponse(long id, String hora_inicio, String hora_fin,
                                    Date fecha_presentacion, Date fecha_finalizacion,
                                    byte tipo_examen, String plan_de_estudios, String codigo_materia,
                                    String departamento_materia, String nombre_materia, String grupo,
                                    byte semestre_pensum, String salon) {
        this.id = id;
        this.hora_inicio = hora_inicio;
        this.hora_fin = hora_fin;
        this.fecha_presentacion = fecha_presentacion;
        this.fecha_finalizacion = fecha_finalizacion;
        this.tipo_examen = tipo_examen;
        this.plan_de_estudios = plan_de_estudios;
        this.codigo_materia = codigo_materia;
        this.departamento_materia = departamento_materia;
        this.nombre_materia = nombre_materia;
        this.grupo = grupo;
        this.semestre_pensum = semestre_pensum;
        this.salon = salon;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getHora_inicio() {
        return hora_inicio;
    }

    public void setHora_inicio(String hora_inicio) {
        this.hora_inicio = hora_inicio;
    }

    public String getHora_fin() {
        return hora_fin;
    }

    public void setHora_fin(String hora_fin) {
        this.hora_fin = hora_fin;
    }

    public Date getFecha_presentacion() {
        return fecha_presentacion;
    }

    public void setFecha_presentacion(Date fecha_presentacion) {
        this.fecha_presentacion = fecha_presentacion;
    }

    public Date getFecha_finalizacion() {
        return fecha_finalizacion;
    }

    public void setFecha_finalizacion(Date fecha_finalizacion) {
        this.fecha_finalizacion = fecha_finalizacion;
    }

    public byte getTipo_examen() {
        return tipo_examen;
    }

    public void setTipo_examen(byte tipo_examen) {
        this.tipo_examen = tipo_examen;
    }

    public String getPlan_de_estudios() {
        return plan_de_estudios;
    }

    public void setPlan_de_estudios(String plan_de_estudios) {
        this.plan_de_estudios = plan_de_estudios;
    }

    public String getCodigo_materia() {
        return codigo_materia;
    }

    public void setCodigo_materia(String codigo_materia) {
        this.codigo_materia = codigo_materia;
    }

    public String getDepartamento_materia() {
        return departamento_materia;
    }

    public void setDepartamento_materia(String departamento_materia) {
        this.departamento_materia = departamento_materia;
    }

    public String getNombre_materia() {
        return nombre_materia;
    }

    public void setNombre_materia(String nombre_materia) {
        this.nombre_materia = nombre_materia;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public byte getSemestre_pensum() {
        return semestre_pensum;
    }

    public void setSemestre_pensum(byte semestre_pensum) {
        this.semestre_pensum = semestre_pensum;
    }

    public String getSalon() {
        return salon;
    }

    public void setSalon(String salon) {
        this.salon = salon;
    }

    @Override
    public String toString() {
        return "ExamenCalendarioResponse{" +
                "id=" + id +
                ", hora_inicio='" + hora_inicio + '\'' +
                ", hora_fin='" + hora_fin + '\'' +
                ", fecha_presentacion=" + fecha_presentacion +
                ", tipo_examen=" + tipo_examen +
                ", plan_de_estudios='" + plan_de_estudios + '\'' +
                ", codigo_materia='" + codigo_materia + '\'' +
                ", departamento_materia='" + departamento_materia + '\'' +
                ", nombre_materia='" + nombre_materia + '\'' +
                ", grupo='" + grupo + '\'' +
                ", semestre_pensum=" + semestre_pensum +
                '}';
    }
}
