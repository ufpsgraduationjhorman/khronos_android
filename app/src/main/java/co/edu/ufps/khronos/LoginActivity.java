package co.edu.ufps.khronos;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Map;

import co.edu.ufps.khronos.adapters.RestAPIAdapter;
import co.edu.ufps.khronos.models.UsuarioResponse;
import co.edu.ufps.khronos.restapi.ConstantesRestAPI;
import co.edu.ufps.khronos.restapi.Endpoints;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 0;
    private static final String TAG = "LoginActivity";
    private UsuarioResponse usuarioLogin;
    private Spinner spinnerRol;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar actionBar = (Toolbar) findViewById(R.id.actionBar);
        setSupportActionBar(actionBar);

        spinnerRol = (Spinner) findViewById(R.id.spinnerTipoUsuario);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.array_rol_usuario, R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerRol.setAdapter(adapter);

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(ConstantesRestAPI.SERVER_CLIENT_ID)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        final GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //.enableAutoManage(this /* FragmentActivity */,
                //      (GoogleApiClient.OnConnectionFailedListener) this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        // Verificar si el usuario ya está logueado.
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        }


        /*Log.d("UmmmAAS", googleApiClient.toString());
        if (validarUsuarioLogueado(googleApiClient)){
            updateUI(true);
        }
        else {*/
            SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
            signInButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.sign_in_button:
                            signIn(googleApiClient);
                            break;
                    }
                }
            });
        //}
    }

    private boolean validarUsuarioLogueado(GoogleApiClient googleApiClient){
        //return googleApiClient != null && googleApiClient.isConnected();
        return prefs.getBoolean("estaLogueado", false); // get value of last login status
    }

    private void signIn(GoogleApiClient googleApiClient) {
        if (googleApiClient.hasConnectedApi(Auth.GOOGLE_SIGN_IN_API)) {
            googleApiClient.clearDefaultAccountAndReconnect();
        }
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            try {
                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount userAccount = result.getSignInAccount();
                String firebaseTokenDispositivo = this.getTokenFirebase();
                this.realizarBackendLogin(userAccount, firebaseTokenDispositivo);
            } catch (Exception e) {
                Log.i("Error Google Auth", e.getMessage());
                Toast.makeText(this, getResources().getString(R.string.error_login), Toast.LENGTH_SHORT).show();
            }
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }

    public String getTokenFirebase() {
        return FirebaseInstanceId.getInstance().getToken();
    }

    private void realizarBackendLogin(GoogleSignInAccount userAccount, String firebaseToken) {
        Log.d("TOKEN OBTENIDO", firebaseToken);
        RestAPIAdapter restApi = new RestAPIAdapter();
        Endpoints endpoints = restApi.inicializarConexionRestAPI();
        String idToken = userAccount.getIdToken();
        Log.d("IDTOK", idToken);
        final String rolUsuario = this.getRolValor(String.valueOf(spinnerRol.getSelectedItem().toString()));

        Call<UsuarioResponse> usuarioResponseCall = endpoints.autenticarUsuario(idToken,
                firebaseToken,
                rolUsuario,
                "android");
        usuarioResponseCall.enqueue(new Callback<UsuarioResponse>() {
            @Override
            public void onResponse(Call<UsuarioResponse> call, Response<UsuarioResponse> response) {
                if (response.isSuccessful()) {
                    usuarioLogin = response.body();
                    prefs.edit().putBoolean("estaLogueado", true).apply(); //commit();
                    prefs.edit().putString("nombreUsuario", usuarioLogin.getNombres()).apply();
                    prefs.edit().putString("authTokenApp", usuarioLogin.getAuth_token_app()).apply();
                    prefs.edit().putInt("tipoIngreso", usuarioLogin.getIngreso_tipo()).apply();
                    updateUI(true);
                }
                else {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.error_login), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UsuarioResponse> call, Throwable t) {
                Log.e("FALLA EN REQUEST", t + "");
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.error_login), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String getRolValor (String seleccion){
        Map<String, String> map = new HashMap<>();
        map.put("estudiante", "1");
        map.put("docente", "2");
        return map.get(seleccion.toLowerCase());

    }

    public void updateUI(boolean status) {
        if (status) {
            Intent intent = new Intent(LoginActivity.this, CalendarioPrincipalActivity.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, getResources().getString(R.string.error_login), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

}