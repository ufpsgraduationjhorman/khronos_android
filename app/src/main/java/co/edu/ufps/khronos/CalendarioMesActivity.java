package co.edu.ufps.khronos;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.edu.ufps.khronos.adapters.RestAPIAdapter;
import co.edu.ufps.khronos.decorators.EventDecorator;
import co.edu.ufps.khronos.models.ExamenCalendarioResponse;
import co.edu.ufps.khronos.restapi.Endpoints;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CalendarioMesActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnDateSelectedListener{

    private static final int RC_SIGN_IN = 0;
    private static final String TAG = "LoginActivity";
    private List<ExamenCalendarioResponse> examenesCalendario;
    private MaterialCalendarView widget;
    private SharedPreferences prefs;
    private Map<Byte, Integer> infoColores = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mes_calendario);

        infoColores.put((byte) 1, R.color.colorPrimerosPrevios);
        infoColores.put((byte) 2, R.color.colorSegundosPrevios);
        infoColores.put((byte) 3, R.color.colorExamenesFinales);
        infoColores.put((byte) 4, R.color.colorHabilitacionesOpcionales);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        widget = (MaterialCalendarView) findViewById(R.id.calendarioExamenesView);
        Toolbar actionBar = (Toolbar) findViewById(R.id.actionBar);

        setSupportActionBar(actionBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        actionBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCalendarioPrincipalActivity();
            }
        });

        this.leerCalendarioExamenes();

        //TextView mStatusTextView = (TextView) findViewById(R.id.mStatusTextView);
        //String info = getResources().getString(R.string.signed_in_fmt) + userAccount.getDisplayName();
        //mStatusTextView.setText(info);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if(keyCode == KeyEvent.KEYCODE_BACK){
            openCalendarioPrincipalActivity();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void openCalendarioPrincipalActivity() {
        Intent intentMain = new Intent(this, CalendarioPrincipalActivity.class);
        startActivity(intentMain);
    }

    private void renderCalendario(){

        this.widget.setOnDateChangedListener(this);
        this.widget.setShowOtherDates(MaterialCalendarView.SHOW_ALL);

        Calendar instance = Calendar.getInstance();
        this.widget.setSelectedDate(instance.getTime());

        Calendar instance1 = Calendar.getInstance();
        instance1.set(instance1.get(Calendar.YEAR), Calendar.JANUARY, 1);

        Calendar instance2 = Calendar.getInstance();
        instance2.set(instance2.get(Calendar.YEAR), Calendar.DECEMBER, 31);

        ArrayList<CalendarDay> dates = new ArrayList<>();

        for (ExamenCalendarioResponse examen: examenesCalendario) {
            CalendarDay day = CalendarDay.from(examen.getFecha_presentacion().getYear() + 1900,
                    examen.getFecha_presentacion().getMonth(),
                    examen.getFecha_presentacion().getDate());
            dates.add(day);
        }

        widget.addDecorator(new EventDecorator(Color.RED,
                ContextCompat.getColor(getApplicationContext(), R.color.colorAccent),
                dates));
        widget.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                Log.d("Seleccionado", date.toString());
                //Intent intent = new Intent(getApplicationContext(), ListaExamenesActivity.class);
                //intent.putExtra("userId", userId);
                //intent.putExtra(getResources().getString(R.string.param_fecha_presentacion), date);
                Intent intent = new Intent(CalendarioMesActivity.this,
                        CalendarioPrincipalActivity.class);
                intent.putExtra("fechaSeleccionada", date.getCalendar());
                startActivity(intent);
                finish();
            }
        });

    }

    private void leerCalendarioExamenes() {
        RestAPIAdapter restApi = new RestAPIAdapter();
        Endpoints endpoints = restApi.inicializarConexionRestAPI();

        Call<ExamenCalendarioResponse[]> examenCalendarioResponseCall = endpoints.leerCalendario(
                prefs.getString("authTokenApp", "")
        );
        examenCalendarioResponseCall.enqueue(new Callback<ExamenCalendarioResponse[]>() {
            @Override
            public void onResponse(Call<ExamenCalendarioResponse[]> call,
                                   Response<ExamenCalendarioResponse[]> response) {
                Log.d("Response cal", response.toString());
                if (response.isSuccessful()){
                    examenesCalendario = Arrays.asList(response.body());
                    renderCalendario();
                    //setupRecyclerView((RecyclerView) recyclerView);
                    //progessBar.setVisibility(View.GONE);
                }
                else {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.error_calendario), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ExamenCalendarioResponse[]> call, Throwable t) {
                Log.e("FALLA REQ CALENDAR", t + "");
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.error_calendario), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        //If you change a decorate, you need to invalidate decorators
        //oneDayDecorator.setDate(date.getDate());
        widget.invalidateDecorators();
    }

}
