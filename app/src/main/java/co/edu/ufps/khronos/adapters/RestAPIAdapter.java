package co.edu.ufps.khronos.adapters;

import android.util.Log;

import co.edu.ufps.khronos.restapi.ConstantesRestAPI;
import co.edu.ufps.khronos.restapi.Endpoints;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jhorapb on 22/11/17.
 */

public class RestAPIAdapter {

    public Endpoints inicializarConexionRestAPI(){
        Log.i("Vamos", "Inicializando conexión");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstantesRestAPI.ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                ;
        return retrofit.create(Endpoints.class);
    }
}
