package co.edu.ufps.khronos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import co.edu.ufps.khronos.adapters.ListaExamenesRecyclerAdapter;
import co.edu.ufps.khronos.adapters.RestAPIAdapter;
import co.edu.ufps.khronos.models.ExamenCalendarioResponse;
import co.edu.ufps.khronos.restapi.Endpoints;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListaExamenesActivity extends AppCompatActivity {

    private List<ExamenCalendarioResponse> examenesFecha;
    private long userId;
    private CalendarDay fecha;
    private RecyclerView listaExamenesRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_examenes_lista);

        Toolbar actionBar = (Toolbar) findViewById(R.id.actionBar);
        setSupportActionBar(actionBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        actionBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInicioActivity();
            }
        });

        listaExamenesRecycler = (RecyclerView) findViewById(R.id.recyclerViewListaExamenes);
        //listaExamenesRecycler.setHasFixedSize(true);

        Bundle params = getIntent().getExtras();
        this.userId = params.getLong("userId");
        this.fecha = (CalendarDay) params.get(getResources().getString(R.string.param_fecha_presentacion));

        this.leerExamenesFecha();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            openInicioActivity();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void leerExamenesFecha() {
        RestAPIAdapter restApi = new RestAPIAdapter();
        Endpoints endpoints = restApi.inicializarConexionRestAPI();
        SimpleDateFormat formatoAMD = new SimpleDateFormat("yyyy-MM-dd");
        String fechaFormateada = formatoAMD.format(fecha.getDate());
        Call<ExamenCalendarioResponse[]> examenCalendarioResponseCall = endpoints.leerExamenesFecha(
                userId, fechaFormateada);
        examenCalendarioResponseCall.enqueue(new Callback<ExamenCalendarioResponse[]>() {
            @Override
            public void onResponse(Call<ExamenCalendarioResponse[]> call,
                                   Response<ExamenCalendarioResponse[]> response) {
                Log.d("Response lista", response.toString());
                if (response.isSuccessful()){
                    examenesFecha = Arrays.asList(response.body());
                    //renderExamenesFecha();
                    renderExamenesFechaRecyclerView();
                }
                else {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.error_calendario), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ExamenCalendarioResponse[]> call, Throwable t) {
                Log.e("FALLA REQ LISTA", t + "");
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.error_calendario), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*
    private void renderExamenesFecha(){

        //contacts.add(new Contact("Valeria Pérez", "3107999058", "valeria@gmail.com", 2));

        ListView listaExamenes = (ListView) findViewById(R.id.listaExamenes);
        ArrayList<String> infoExamenes = new ArrayList<>();

        for (ExamenCalendarioResponse examen :
                examenesFecha) {
            infoExamenes.add(String.format(Locale.getDefault(), "%s%s-%s %s",
                    examen.getPlan_de_estudios(), examen.getCodigo_materia(),
                    examen.getGrupo(),
                    examen.getNombre_materia()));
        }
        listaExamenes.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                infoExamenes));

        listaExamenes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ListaExamenesActivity.this, DetalleExamenActivity.class);
                ExamenCalendarioResponse examen = examenesFecha.get(position);
                String nombreExamen = String.format(Locale.getDefault(), "%s%s-%s %s",
                        examen.getPlan_de_estudios(), examen.getCodigo_materia(),
                        examen.getGrupo(),
                        examen.getNombre_materia());
                intent.putExtra(getResources().getString(R.string.param_examen_materia),
                        nombreExamen);
                intent.putExtra(getResources().getString(R.string.param_examen_hora_inicio),
                        examen.getHora_inicio());
                intent.putExtra(getResources().getString(R.string.param_examen_hora_fin),
                        examen.getHora_fin());
                startActivity(intent);
                finish();
            }
        });
    }*/

    private void renderExamenesFechaRecyclerView(){

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        listaExamenesRecycler.setLayoutManager(layoutManager);
        listaExamenesRecycler.setAdapter(new ListaExamenesRecyclerAdapter(examenesFecha));
        listaExamenesRecycler.addItemDecoration(
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        /*
        listaExamenesRecycler.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ListaExamenesActivity.this, DetalleExamenActivity.class);
                ExamenCalendarioResponse examen = examenesFecha.get(position);
                String nombreExamen = String.format(Locale.getDefault(), "%s%s-%s %s",
                        examen.getPlan_de_estudios(), examen.getCodigo_materia(),
                        examen.getGrupo(),
                        examen.getNombre_materia());
                intent.putExtra(getResources().getString(R.string.param_examen_materia),
                        nombreExamen);
                intent.putExtra(getResources().getString(R.string.param_examen_hora_inicio),
                        examen.getHora_inicio());
                intent.putExtra(getResources().getString(R.string.param_examen_hora_fin),
                        examen.getHora_fin());
                startActivity(intent);
                finish();
            }
        });*/
    }

    private void openInicioActivity() {
        Intent intentMain = new Intent(this, CalendarioMesActivity.class);
        startActivity(intentMain);
    }
}
