package co.edu.ufps.khronos.models;

/**
 * Created by jhorapb on 22/11/17.
 */

public class UsuarioResponse {

    private long id;
    private String correo;
    private String nombres;
    private String apellidos;
    private byte ingreso_tipo;
    private String auth_token_app;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    private String plan_de_estudios;
    private String codigo;

    public UsuarioResponse() {

    }

    public UsuarioResponse(long id, String correo, String nombres, String apellidos,
                           byte ingreso_tipo, String auth_token_app, String plan_de_estudios,
                           String codigo) {
        this.id = id;
        this.correo = correo;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.ingreso_tipo = ingreso_tipo;
        this.auth_token_app = auth_token_app;
        this.plan_de_estudios = plan_de_estudios;
        this.codigo = codigo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public byte getIngreso_tipo() {
        return ingreso_tipo;
    }

    public void setIngreso_tipo(byte ingreso_tipo) {
        this.ingreso_tipo = ingreso_tipo;
    }

    public String getPlan_de_estudios() {
        return plan_de_estudios;
    }

    public void setPlan_de_estudios(String plan_de_estudios) {
        this.plan_de_estudios = plan_de_estudios;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getAuth_token_app() {
        return auth_token_app;
    }

    public void setAuth_token_app(String auth_token_app) {
        this.auth_token_app = auth_token_app;
    }

    @Override
    public String toString() {
        return "UsuarioResponse{" +
                "id=" + id +
                ", correo='" + correo + '\'' +
                ", nombres='" + nombres + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", ingreso_tipo=" + ingreso_tipo +
                ", auth_token_app='" + auth_token_app + '\'' +
                ", plan_de_estudios='" + plan_de_estudios + '\'' +
                ", codigo='" + codigo + '\'' +
                '}';
    }
}